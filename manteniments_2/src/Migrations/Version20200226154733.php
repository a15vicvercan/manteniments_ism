<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226154733 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo ADD antivirus_id INT DEFAULT NULL, DROP antivirus');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530B26A1082F FOREIGN KEY (antivirus_id) REFERENCES antivirus (id)');
        $this->addSql('CREATE INDEX IDX_C49C530B26A1082F ON equipo (antivirus_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo DROP FOREIGN KEY FK_C49C530B26A1082F');
        $this->addSql('DROP INDEX IDX_C49C530B26A1082F ON equipo');
        $this->addSql('ALTER TABLE equipo ADD antivirus VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP antivirus_id');
    }
}
