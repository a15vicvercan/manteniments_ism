<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226154211 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo ADD ram_id INT DEFAULT NULL, DROP ram');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530B3366068 FOREIGN KEY (ram_id) REFERENCES ram (id)');
        $this->addSql('CREATE INDEX IDX_C49C530B3366068 ON equipo (ram_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo DROP FOREIGN KEY FK_C49C530B3366068');
        $this->addSql('DROP INDEX IDX_C49C530B3366068 ON equipo');
        $this->addSql('ALTER TABLE equipo ADD ram VARCHAR(50) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP ram_id');
    }
}
