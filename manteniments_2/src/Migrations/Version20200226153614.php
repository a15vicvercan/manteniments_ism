<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226153614 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo ADD placas_base_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530B5A5A0872 FOREIGN KEY (placas_base_id) REFERENCES placas_base (id)');
        $this->addSql('CREATE INDEX IDX_C49C530B5A5A0872 ON equipo (placas_base_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo DROP FOREIGN KEY FK_C49C530B5A5A0872');
        $this->addSql('DROP INDEX IDX_C49C530B5A5A0872 ON equipo');
        $this->addSql('ALTER TABLE equipo DROP placas_base_id');
    }
}
