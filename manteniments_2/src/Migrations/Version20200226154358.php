<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200226154358 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo ADD disco_duro_id INT DEFAULT NULL, DROP disco_duro');
        $this->addSql('ALTER TABLE equipo ADD CONSTRAINT FK_C49C530B7B483811 FOREIGN KEY (disco_duro_id) REFERENCES disco_duro (id)');
        $this->addSql('CREATE INDEX IDX_C49C530B7B483811 ON equipo (disco_duro_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE equipo DROP FOREIGN KEY FK_C49C530B7B483811');
        $this->addSql('DROP INDEX IDX_C49C530B7B483811 ON equipo');
        $this->addSql('ALTER TABLE equipo ADD disco_duro VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`, DROP disco_duro_id');
    }
}
