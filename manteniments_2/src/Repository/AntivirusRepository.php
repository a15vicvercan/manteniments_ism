<?php

namespace App\Repository;

use App\Entity\Antivirus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Antivirus|null find($id, $lockMode = null, $lockVersion = null)
 * @method Antivirus|null findOneBy(array $criteria, array $orderBy = null)
 * @method Antivirus[]    findAll()
 * @method Antivirus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AntivirusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Antivirus::class);
    }

    // /**
    //  * @return Antivirus[] Returns an array of Antivirus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Antivirus
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
