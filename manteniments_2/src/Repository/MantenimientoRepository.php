<?php

namespace App\Repository;

use App\Entity\Mantenimiento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Mantenimiento|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mantenimiento|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mantenimiento[]    findAll()
 * @method Mantenimiento[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MantenimientoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mantenimiento::class);
    }

    // /**
    //  * @return Mantenimiento[] Returns an array of Mantenimiento objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mantenimiento
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
