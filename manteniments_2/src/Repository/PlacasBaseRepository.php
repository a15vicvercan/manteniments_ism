<?php

namespace App\Repository;

use App\Entity\PlacasBase;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PlacasBase|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlacasBase|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlacasBase[]    findAll()
 * @method PlacasBase[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlacasBaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlacasBase::class);
    }

    // /**
    //  * @return PlacasBase[] Returns an array of PlacasBase objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlacasBase
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
