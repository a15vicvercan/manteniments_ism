<?php

namespace App\Repository;

use App\Entity\DiscoDuro;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DiscoDuro|null find($id, $lockMode = null, $lockVersion = null)
 * @method DiscoDuro|null findOneBy(array $criteria, array $orderBy = null)
 * @method DiscoDuro[]    findAll()
 * @method DiscoDuro[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DiscoDuroRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DiscoDuro::class);
    }

    // /**
    //  * @return DiscoDuro[] Returns an array of DiscoDuro objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DiscoDuro
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
