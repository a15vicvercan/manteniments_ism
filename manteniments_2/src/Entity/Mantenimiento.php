<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MantenimientoRepository")
 */
class Mantenimiento
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tecnico;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $albaran;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cliente", inversedBy="mantenimientos")
     * @ORM\JoinColumn(nullable=false)
     */
    private $cliente;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $horaEntrada;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $horaSalida;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mes_mantenimiento;

    /**
     * @ORM\Column(type="float")
     */
    private $horasTotales;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $dia;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mes;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ano;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ano_mantenimiento;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(?string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getTecnico(): ?string
    {
        return $this->tecnico;
    }

    public function setTecnico(?string $tecnico): self
    {
        $this->tecnico = $tecnico;

        return $this;
    }

    public function getAlbaran(): ?string
    {
        return $this->albaran;
    }

    public function setAlbaran(?string $albaran): self
    {
        $this->albaran = $albaran;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getHoraEntrada(): ?string
    {
        return $this->horaEntrada;
    }

    public function setHoraEntrada(string $horaEntrada): self
    {
        $this->horaEntrada = $horaEntrada;

        return $this;
    }

    public function getHoraSalida(): ?string
    {
        return $this->horaSalida;
    }

    public function setHoraSalida(string $horaSalida): self
    {
        $this->horaSalida = $horaSalida;

        return $this;
    }

    public function getMesMantenimiento(): ?string
    {
        return $this->mes_mantenimiento;
    }

    public function setMesMantenimiento(string $mes_mantenimiento): self
    {
        $this->mes_mantenimiento = $mes_mantenimiento;

        return $this;
    }

    public function getHorasTotales(): ?float
    {
        return $this->horasTotales;
    }

    public function setHorasTotales(float $horasTotales): self
    {
        $this->horasTotales = $horasTotales;

        return $this;
    }

    public function getDia(): ?string
    {
        return $this->dia;
    }

    public function setDia(string $dia): self
    {
        $this->dia = $dia;

        return $this;
    }

    public function getMes(): ?string
    {
        return $this->mes;
    }

    public function setMes(string $mes): self
    {
        $this->mes = $mes;

        return $this;
    }

    public function getAno(): ?string
    {
        return $this->ano;
    }

    public function setAno(string $ano): self
    {
        $this->ano = $ano;

        return $this;
    }

    public function getAnoMantenimiento(): ?string
    {
        return $this->ano_mantenimiento;
    }

    public function setAnoMantenimiento(string $ano_mantenimiento): self
    {
        $this->ano_mantenimiento = $ano_mantenimiento;

        return $this;
    }
}
