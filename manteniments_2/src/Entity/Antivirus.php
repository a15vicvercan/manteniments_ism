<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AntivirusRepository")
 */
class Antivirus
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Equipo", mappedBy="antivirus")
     */
    private $equipo;

    public function __construct()
    {
        $this->equipo = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return Collection|Equipo[]
     */
    public function getEquipo(): Collection
    {
        return $this->equipo;
    }

    public function addEquipo(Equipo $equipo): self
    {
        if (!$this->equipo->contains($equipo)) {
            $this->equipo[] = $equipo;
            $equipo->setAntivirus($this);
        }

        return $this;
    }

    public function removeEquipo(Equipo $equipo): self
    {
        if ($this->equipo->contains($equipo)) {
            $this->equipo->removeElement($equipo);
            // set the owning side to null (unless already changed)
            if ($equipo->getAntivirus() === $this) {
                $equipo->setAntivirus(null);
            }
        }

        return $this;
    }
}
