<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CalendarioRepository")
 */
class Calendario
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Cliente", inversedBy="calendario", cascade={"persist", "remove"})
     */
    private $cliente;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $enero;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $febrero;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $marzo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getEnero(): ?float
    {
        return $this->enero;
    }

    public function setEnero(?float $enero): self
    {
        $this->enero = $enero;

        return $this;
    }

    public function getFebrero(): ?float
    {
        return $this->febrero;
    }

    public function setFebrero(?float $febrero): self
    {
        $this->febrero = $febrero;

        return $this;
    }

    public function getMarzo(): ?float
    {
        return $this->marzo;
    }

    public function setMarzo(?float $marzo): self
    {
        $this->marzo = $marzo;

        return $this;
    }
}
