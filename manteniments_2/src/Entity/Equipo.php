<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EquipoRepository")
 */
class Equipo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $usuario;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zona;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ip;

    /**
     * @ORM\Column(type="string", length=5000, nullable=true)
     */
    private $observaciones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cliente", inversedBy="equipos")
     */
    private $cliente;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licenciaSo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licenciaOffice;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $placaBase;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contrasena;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $alias;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\PlacasBase", inversedBy="equipo")
     */
    private $placasBase;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cpu", inversedBy="equipo")
     */
    private $cpu;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Ram", inversedBy="equipo")
     */
    private $ram;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\DiscoDuro", inversedBy="equipo")
     */
    private $discoDuro;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SistemaOperativo", inversedBy="equipo")
     */
    private $sistemaOperativo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Office", inversedBy="equipo")
     */
    private $office;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Antivirus", inversedBy="equipo")
     */
    private $antivirus;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tipo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getUsuario(): ?string
    {
        return $this->usuario;
    }

    public function setUsuario(string $usuario): self
    {
        $this->usuario = $usuario;

        return $this;
    }

    public function getZona(): ?string
    {
        return $this->zona;
    }

    public function setZona(?string $zona): self
    {
        $this->zona = $zona;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getLicenciaSo(): ?string
    {
        return $this->licenciaSo;
    }

    public function setLicenciaSo(?string $licenciaSo): self
    {
        $this->licenciaSo = $licenciaSo;

        return $this;
    }

    public function getLicenciaOffice(): ?string
    {
        return $this->licenciaOffice;
    }

    public function setLicenciaOffice(?string $licenciaOffice): self
    {
        $this->licenciaOffice = $licenciaOffice;

        return $this;
    }

    public function getPlacaBase(): ?string
    {
        return $this->placaBase;
    }

    public function setPlacaBase(?string $placaBase): self
    {
        $this->placaBase = $placaBase;

        return $this;
    }

    public function getContrasena(): ?string
    {
        return $this->contrasena;
    }

    public function setContrasena(?string $contrasena): self
    {
        $this->contrasena = $contrasena;

        return $this;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }

    public function getPlacasBase(): ?PlacasBase
    {
        return $this->placasBase;
    }

    public function setPlacasBase(?PlacasBase $placasBase): self
    {
        $this->placasBase = $placasBase;

        return $this;
    }

    public function getCpu(): ?Cpu
    {
        return $this->cpu;
    }

    public function setCpu(?Cpu $cpu): self
    {
        $this->cpu = $cpu;

        return $this;
    }

    public function getRam(): ?Ram
    {
        return $this->ram;
    }

    public function setRam(?Ram $ram): self
    {
        $this->ram = $ram;

        return $this;
    }

    public function getDiscoDuro(): ?DiscoDuro
    {
        return $this->discoDuro;
    }

    public function setDiscoDuro(?DiscoDuro $discoDuro): self
    {
        $this->discoDuro = $discoDuro;

        return $this;
    }

    public function getSistemaOperativo(): ?SistemaOperativo
    {
        return $this->sistemaOperativo;
    }

    public function setSistemaOperativo(?SistemaOperativo $sistemaOperativo): self
    {
        $this->sistemaOperativo = $sistemaOperativo;

        return $this;
    }

    public function getOffice(): ?Office
    {
        return $this->office;
    }

    public function setOffice(?Office $office): self
    {
        $this->office = $office;

        return $this;
    }

    public function getAntivirus(): ?Antivirus
    {
        return $this->antivirus;
    }

    public function setAntivirus(?Antivirus $antivirus): self
    {
        $this->antivirus = $antivirus;

        return $this;
    }

    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    public function setTipo(?string $tipo): self
    {
        $this->tipo = $tipo;

        return $this;
    }

}
