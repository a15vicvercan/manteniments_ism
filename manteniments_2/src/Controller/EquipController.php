<?php


namespace App\Controller;

use App\Entity\Equipo;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EquipController extends AbstractController
{

    //Funcion que muestra la informacion de cada equipo
    /**
     * @Route("/llistar_clients/{cliente}/equips/{equipo}", name="info_equip")
     */
    public function mostrar_equipo($cliente, $equipo, Request $request) {

        $repository = $this->getDoctrine()->getRepository(Equipo::class);
        $equipo = $repository->findBy(array("id" => $equipo));

        if($equipo) {
            return $this->render("equipo.html.twig", [
                "equipo" => $equipo,
                "cliente" => $cliente,
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha informació d'aquest equip"
            );
        }
    }


    //Funcion que elimina un equipo existente
    /**
     * @Route("/eliminarEquip/{equipId}", name="eliminar_equip")
     */
    public function eliminar_equip($equipId, Request $request) {

        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Equipo::class)->find($equipId);

        $entityManager->remove($product);
        $entityManager->flush();
        
        return $this->redirectToRoute('llistat_clients');
    }


    //Funcion Que crea un nuevo equipo
    /**
     * @Route("/nou_equip", name="nou_equip")
     */
    public function crear_equio(Request $request) {

        $equipo = new Equipo();

        $form = $this->createFormBuilder($equipo)
            ->add("nombre", TextType::class, ["label" => "Nom del equip: "])
            ->add("alias", TextType::class, array("label" => "Alias: ", 'required' => false))
            ->add("tipo", ChoiceType::class, array(
                "label" => "Tipus (servidor o PC): ",
                "choices" => array(
                    "" => "",
                    "Servidor" => "Servidor",
                    "PC" => "PC",
                )
            ))
            ->add("cliente", EntityType::class, array(
                "class" => "App\Entity\Cliente",
                "choice_label" => "nombre",
            ))
            ->add("usuario", TextType::class, ["label" => "Usuari: "])
            ->add("contrasena", TextType::class, array("label" => "Contrasenya: ", "required" => false))
            ->add("placasBase", EntityType::class, array(
                "class" => "App\Entity\PlacasBase",
                "choice_label" => "nombre",
                "label" => "Placa base: "
            ))
            ->add("cpu", EntityType::class, array(
                "class" => "App\Entity\Cpu",
                "choice_label" => "nombre",
                "label" => "CPU: "
            ))
            ->add("ram", EntityType::class, array(
                "class" => "App\Entity\Ram",
                "choice_label" => "nombre",
                "label" => "RAM: "
            ))
            ->add("discoDuro", EntityType::class, array(
                "class" => "App\Entity\DiscoDuro",
                "choice_label" => "nombre",
                "label" => "Disc dur: "
            ))
            ->add("sistemaOperativo", EntityType::class, array(
                "class" => "App\Entity\SistemaOperativo",
                "choice_label" => "nombre",
                "label" => "Sistema operatiu: "
            ))
            ->add("licenciaSo", TextType::class, array("label" => "Llicencia SO: ", "required" => false))
            ->add("office", EntityType::class, array(
                "class" => "App\Entity\Office",
                "choice_label" => "nombre",
                "label" => "Office: "
            ))
            ->add("licenciaOffice", TextType::class, array("label" => "Llicencia Office: ", "required" => false))
            ->add("antivirus", EntityType::class, array(
                "class" => "App\Entity\Antivirus",
                "choice_label" => "nombre",
                "label" => "Antivirus: "
            ))
            ->add("zona", TextType::class, array("label" => "Zona: ", "required" => false))
            ->add("ip", TextType::class, array("label" => "IP: ", "required" => false))
            ->add("observaciones", TextType::class, array("label" => "Observacions: ", "required" => false))
            ->add("submit", SubmitType::class, ["label" => "Crear Equip"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $equipo = $form->getData();
            $entityManager->persist($equipo);
            $entityManager->flush();

            return $this->redirectToRoute('nou_equip');
        }

        return $this->render("nouEquip.html.twig", [
            'form' => $form->createView(),
            'origen' => 'nou_equip'
        ]);
    }

    /**
     * @Route("/llistar_clients/{cliente}/equips/editar/{id}", name="editarEquip")
     */
    public function editarEquip($id, $cliente, Request $request) {
        $entityManager = $this->getDoctrine()->getManager();
        $equip = $entityManager->getRepository(Equipo::class)->find($id);

        $form = $this->createFormBuilder($equip)
            ->add("nombre", TextType::class, ["label" => "Nom del equip: "])
            ->add("tipo", ChoiceType::class, array(
                "label" => "Tipus (servidor o PC): ",
                "choices" => array(
                    "Servidor" => "Servidor",
                    "PC" => "PC",
                )
            ))
            ->add("alias", TextType::class, ["label" => "Alias: "], ["required" => false])
            ->add("cliente", EntityType::class, array(
                "class" => "App\Entity\Cliente",
                "choice_label" => "nombre"
            ))
            ->add("usuario", TextType::class, ["label" => "Usuari: "])
            ->add("contrasena", TextType::class, ["label" => "Contrasenya: "], ["required" => false])
            ->add("placasBase", EntityType::class, array(
                "class" => "App\Entity\PlacasBase",
                "choice_label" => "nombre"
            ))
            ->add("cpu", EntityType::class, array(
                "class" => "App\Entity\Cpu",
                "choice_label" => "nombre"
            ))
            ->add("ram", EntityType::class, array(
                "class" => "App\Entity\Ram",
                "choice_label" => "nombre"
            ))
            ->add("discoDuro", EntityType::class, array(
                "class" => "App\Entity\DiscoDuro",
                "choice_label" => "nombre"
            ))
            ->add("sistemaOperativo", EntityType::class, array(
                "class" => "App\Entity\SistemaOperativo",
                "choice_label" => "nombre"
            ))
            ->add("licenciaSo", TextType::class, ["label" => "Llicencia SO: "], ["required" => false])
            ->add("office", EntityType::class, array(
                "class" => "App\Entity\Office",
                "choice_label" => "nombre"
            ))
            ->add("licenciaOffice", TextType::class, ["label" => "Llicencia Office: "], ["required" => false])
            ->add("antivirus", EntityType::class, array(
                "class" => "App\Entity\Antivirus",
                "choice_label" => "nombre"
            ))
            ->add("zona", TextType::class, ["label" => "Zona: "], ["required" => false])
            ->add("ip", TextType::class, ["label" => "IP: "], ["required" => false])
            ->add("observaciones", TextType::class, ["label" => "Observacions: "], ["required" => false])
            ->add("submit", SubmitType::class, ["label" => "Guardar canvis"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $equip = $form->getData();
            $entityManager->persist($equip);
            $entityManager->flush();

            //return $this->redirectToRoute('llistat_equips');
        }

        return $this->render("editarEquip.html.twig", [
            'form' => $form->createView(),
        ]);
    }
}