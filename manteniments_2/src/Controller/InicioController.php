<?php


namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InicioController extends AbstractController
{
    /**
     * @Route("/inicio", name="inicio")
     */
    public function inicio() {

        return $this->render("inicio.html.twig", [
            "manteniment" => "Nou manteniment",
            "clients" => "Llistat clients",
            "nou_client" => "Nou Client",
            "nou_equip" => "Nou Equip",
            "taula_manteniments" => "Taula manteniments"
        ]);
    }
}
