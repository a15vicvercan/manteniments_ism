<?php


namespace App\Controller;

use App\Entity\Antivirus;
use App\Entity\Cpu;
use App\Entity\DiscoDuro;
use App\Entity\Office;
use App\Entity\PlacasBase;
use App\Entity\Ram;
use App\Entity\SistemaOperativo;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class Components extends AbstractController
{

    /**
     * @Route("/modificar_components", name="modificar_components")
     */
    public function modificarComponents() {

        return $this->render("components.html.twig", [
            "cpu" => "CPU",
            "ram" => "RAM",
            "disc_durs" => "Disc durs",
            "sistemes_operatius" => "Sistemes operatius",
            "office" => "Office",
            "plaques_base" => "Plaques base",
            "antivirus" => "Antivirus"
        ]);
    }


    //Funcion que devuelve un listado con todas las CPUs
    /**
     * @Route("/llistat_cpu", name="llistat_cpu")
     */
    public function mostrar_cpu() {

        $repository = $this->getDoctrine()->getRepository(Cpu::class);
        $cpu = $repository->findAll();

        if($cpu) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $cpu
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap cpu"
            );
        }
    }


    //Funcion que devuelve un listado con todas las ram
    /**
     * @Route("/llistat_ram", name="llistat_ram")
     */
    public function mostrar_ram() {

        $repository = $this->getDoctrine()->getRepository(Ram::class);
        $ram = $repository->findAll();

        if($ram) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $ram
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap ram al sistema"
            );
        }
    }


    //Funcion que devuelve un listado con todos los discos duros
    /**
     * @Route("/llistat_disc_dur", name="llistat_disc_dur")
     */
    public function mostrar_disc_dur() {

        $repository = $this->getDoctrine()->getRepository(DiscoDuro::class);
        $disc_dur = $repository->findAll();

        if($disc_dur) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $disc_dur
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap disc dur al sistema"
            );
        }
    }


    //Funcion que devuelve un listado con todos los sistemas operativos
    /**
     * @Route("/llistat_sistemes_operatius", name="llistat_sistemes_operatius")
     */
    public function mostrar_sistemes_operatius() {

        $repository = $this->getDoctrine()->getRepository(SistemaOperativo::class);
        $sistemes_operatius = $repository->findAll();

        if($sistemes_operatius) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $sistemes_operatius
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap sistema operatiu al sistema"
            );
        }
    }


    //Funcion que devuelve un listado con todos los office
    /**
     * @Route("/llistat_office", name="llistat_office")
     */
    public function mostrar_office() {

        $repository = $this->getDoctrine()->getRepository(Office::class);
        $office = $repository->findAll();

        if($office) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $office
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap office al sistema"
            );
        }
    }


    //Funcion que devuelve un listado con todas las placas base
    /**
     * @Route("/llistat_plaques_base", name="llistat_plaques_base")
     */
    public function mostrar_plaques_base() {

        $repository = $this->getDoctrine()->getRepository(PlacasBase::class);
        $plaques_base = $repository->findAll();

        if($plaques_base) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $plaques_base
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap placa base al sistema"
            );
        }
    }

    /**
     * @Route("/eliminar_placaBase/{nombre}", name="eliminar_antivirus")
     */
    public function eliminar_placaBase($nombre) {

        $entityManager = $this->getDoctrine()->getManager();

        $placaBase = $this->getDoctrine()
            ->getRepository(PlacasBase::class)
            ->createQueryBuilder("placaBase")
            ->addSelect("placaBase")
            ->andWhere("placaBase.nombre = :nombre")
            ->setParameter("nombre", $nombre)
            ->getQuery()
            ->getResult();

        if($placaBase) {
            $entityManager->remove($placaBase[0]);
            $entityManager->flush();

            $repository = $this->getDoctrine()->getRepository(PlacasBase::class);
            $antivirus = $repository->findAll();

            return $this->render("listarComponente.html.twig", [
                "componente" => $antivirus
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap antivirus al sistema"
            );
        }
    }


    //Funcion que devuelve un listado con todos los antivirus
    /**
     * @Route("/llistat_antivirus", name="llistat_antivirus")
     */
    public function mostrar_antivirus() {

        $repository = $this->getDoctrine()->getRepository(Antivirus::class);
        $antivirus = $repository->findAll();

        if($antivirus) {
            return $this->render("listarComponente.html.twig", [
                "componente" => $antivirus
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap antivirus al sistema"
            );
        }
    }


    //Funcion que elimina el antivirus selecionado
    /**
     * @Route("/eliminar_antivirus/{nombre}", name="eliminar_antivirus")
     */
    public function eliminar_antivirus($nombre) {

        $entityManager = $this->getDoctrine()->getManager();

        $antivirus = $this->getDoctrine()
            ->getRepository(Antivirus::class)
            ->createQueryBuilder("antivirus")
            ->addSelect("antivirus")
            ->andWhere("antivirus.nombre = :nombre")
            ->setParameter("nombre", $nombre)
            ->getQuery()
            ->getResult();

        if($antivirus) {
            $entityManager->remove($antivirus[0]);
            $entityManager->flush();

            $repository = $this->getDoctrine()->getRepository(Antivirus::class);
            $antivirus = $repository->findAll();

            return $this->render("listarComponente.html.twig", [
                "componente" => $antivirus
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap antivirus al sistema"
            );
        }
    }



    /**
     * @Route("/afegir_component", name="anadir_componente")
     */
    public function mostrar_component(Request $request) {

        $form = $this->createFormBuilder()
            ->add("nombre", TextType::class, ["label" => "Nom del component: "])
            ->add("componente", ChoiceType::class, [
                "choices" => [
                    "CPU" => "cpu",
                    "RAM" => "ram",
                    "Disc dur" => "disco_duro",
                    "Sistemes operatius" => "sistema_operativo",
                    "Office" => "office",
                    "Plaques base" => "placas_base",
                    "Antivirus" => "antivirus"
            ]], ["label" => "Categoria: "])
            ->add("submit", SubmitType::class, ["label" => "Guardar component"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $choix = $form->get("componente")->getData();

            $entityManager = $this->getDoctrine()->getManager();

            if($choix == "cpu") {
                $cpu = new Cpu();
                $cpu->setNombre($form->get("nombre")->getData());

                $entityManager->persist($cpu);

                $entityManager->flush();
            }
            else if($choix == "ram") {
                $ram = new Ram();
                $ram->setNombre($form->get("nombre")->getData());

                $entityManager->persist($ram);

                $entityManager->flush();
            }
            else if($choix == "disco_duro") {
                $disco_duro = new DiscoDuro();
                $disco_duro->setNombre($form->get("nombre")->getData());

                $entityManager->persist($disco_duro);

                $entityManager->flush();
            }
            else if($choix == "sistema_operativo") {
                $sistema_operativo = new SistemaOperativo();
                $sistema_operativo->setNombre($form->get("nombre")->getData());

                $entityManager->persist($sistema_operativo);

                $entityManager->flush();
            }
            else if($choix == "office") {
                $office = new Office();
                $office->setNombre($form->get("nombre")->getData());

                $entityManager->persist($office);

                $entityManager->flush();
            }
            else if($choix == "placas_base") {
                $placas_base = new PlacasBase();
                $placas_base->setNombre($form->get("nombre")->getData());

                $entityManager->persist($placas_base);

                $entityManager->flush();
            }
            else if($choix == "antivirus") {
                $antivirus = new Antivirus();
                $antivirus->setNombre($form->get("nombre")->getData());

                $entityManager->persist($antivirus);

                $entityManager->flush();
            }

            return $this->redirectToRoute('modificar_components');
        }

        return $this->render("nouComponent.html.twig", [
            'form' => $form->createView(),
        ]);

    }




}