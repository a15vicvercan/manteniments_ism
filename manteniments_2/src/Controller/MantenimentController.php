<?php


namespace App\Controller;


use App\Entity\Cliente;
use App\Entity\Equipo;
use App\Entity\Mantenimiento;
use Doctrine\DBAL\Types\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Choice;

class MantenimentController extends AbstractController
{

    //Funcion que muestra la informacion de cada mantenimiento
    /**
     * @Route("/llistar_clients/{cliente}/manteniments/{id}", name="mostrar_mantenimiento")
     */
    public function mostrar_mantenimiento($cliente, $id, Request $request) {

        $repository = $this->getDoctrine()->getRepository(Mantenimiento::class);
        $mantenimiento = $repository->findBy(array("id" => $id));

        if($mantenimiento) {
            return $this->render("mantenimiento.html.twig", [
                "mantenimiento" => $mantenimiento,
                "cliente" => $cliente,
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha informació d'aquest equip"
            );
        }
    }

    //Funcion que crea un nuevo mantenimiento
    /**
     * @Route("/nou_manteniment", name="nou_manteniment")
     */
    public function nou_manteniment(Request $request) {

        $mantenimiento = new Mantenimiento();

        $form = $this->createFormBuilder($mantenimiento)
            ->add("cliente", EntityType::class, array(
                "class" => "App\Entity\Cliente",
                "choice_label" => "nombre"
            ))
            ->add("mes_mantenimiento", ChoiceType::class, array(
                "choices" => array(
                    "Gener" => "Gener",
                    "Febrer" => "Febrer",
                    "Març" => "Març",
                    "Abril" => "Abril",
                    "Maig" => "Maig",
                    "Juny" => "Juny",
                    "Juliol" => "Juliol",
                    "Agost" => "Agost",
                    "Setembre" => "Setembre",
                    "Octubre" => "Octubre",
                    "Novembre" => "Novembre",
                    "Desembre" => "Desembre",
                )
            ))
            ->add("ano_mantenimiento", ChoiceType::class, array(
                "choices" => array(
                    "2020" => "2020",
                    "2021" => "2021",
                    "2022" => "2022",
                    "2023" => "2023",
                    "2024" => "2024",
                    "2025" => "2025",
                    "2026" => "2026",
                    "2027" => "2027",
                    "2028" => "2028",
                    "2029" => "2029",
                    "2030" => "2030",
                ),
                "label" => "Any del manteniment: ",
            ))
            ->add("fecha", TextType::class, ["label" => "Data (dd/mm/aaaa): "])
            ->add("horaEntrada", TextType::class, ["label" => "Hora d'entrada: "])
            ->add("horaSalida", TextType::class, ["label" => "Hora de sortida: "])
            ->add("horasTotales", ChoiceType::class, array(
                "choices" => array(
                    "0h 15m" => 0.25,
                    "0h 30m" => 0.5,
                    "0h 45m" =>0.75,
                    "1h" => 1.00,
                    "1h 15m" => 1.25,
                    "1h 30m" => 1.5,
                    "1h 45m" => 1.75,
                    "2h" => 2,
                    "2h 15m" => 2.25,
                    "2h 30m" => 2.5,
                    "2h 45m" => 2.75,
                    "3h" => 3,
                    "3h 15m" => 3.25,
                    "3h 30m" => 3.5,
                    "3h 45m" => 3.75,
                    "4h" => 4,
                    "4h 15m" => 4.25,
                    "4h 30m" => 4.5,
                    "4h 45m" => 4.75,
                    "5h" => 5,
                    "5h 15m" => 5.25,
                    "5h 30m" => 5.5,
                    "5h 45m" => 5.75,
                    "6h" => 6,
                ),
                "label" => "Hores totals: "
            ))
            ->add("tecnico", ChoiceType::class, array(
                "choices" => array(
                    "Oriol" => "Oriol",
                    "Víctor" => "Víctor",
                )
            ))
            ->add("albaran", TextType::class, ["label" => "Albara: ", "required" => false])
            ->add("observaciones", TextareaType::class, ["label" => "Observacions: ", "required" => false])
            ->add("submit", SubmitType::class, ["label" => "Guardar manteniment"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $equipo = $form->getData();
            $entityManager->persist($equipo);
            $entityManager->flush();

            return $this->redirectToRoute('inicio');
        }

        return $this->render("nouManteniment.html.twig", [
            'form' => $form->createView(),
            'origen' => 'nou_manteniment'
        ]);

    }

    /**
     * @Route("/taula_manteniments", name="taulaManteniments")
     */
    public function taulaManteniments(Request $request) {

        $repository = $this->getDoctrine()->getRepository(Cliente::class);

        $clientes = $this->getDoctrine()
            ->getRepository(Cliente::class)
            ->createQueryBuilder("cliente")
            ->addSelect("cliente")
            ->andWhere("cliente.tipusManteniment = :manteniment")
            ->setParameter("manteniment", "Mensual")
            ->getQuery()
            ->getResult();


        if($clientes) {
            return $this->render("tabla_mantenimientos_mensual.html.twig", [
                "clientes" => $clientes
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap client"
            );
        }

    }

    /**
     * @Route("/llistar_clients/{cliente}/manteniments/editarManteniment/{id}", name="editarManteniment")
     */
    public function editarManteniment($cliente, $id, Request $request) {

        $entityManager = $this->getDoctrine()->getManager();
        $mantenimiento = $entityManager->getRepository(Mantenimiento::class)->find($id);

        $form = $this->createFormBuilder($mantenimiento)
            ->add("cliente", EntityType::class, array(
                "class" => "App\Entity\Cliente",
                "choice_label" => "nombre",
                "label" => "Client: ",
            ))
            ->add("mes_mantenimiento", ChoiceType::class, array(
                "choices" => array(
                    "Gener" => "Gener",
                    "Febrer" => "Febrer",
                    "Març" => "Març",
                    "Abril" => "Abril",
                    "Maig" => "Maig",
                    "Juny" => "Juny",
                    "Juliol" => "Juliol",
                    "Agost" => "Agost",
                    "Setembre" => "Setembre",
                    "Octubre" => "Octubre",
                    "Novembre" => "Novembre",
                    "Desembre" => "Desembre",
                ),
                "label" => "Mes del manteniment: ",
            ))
            ->add("ano_mantenimiento", ChoiceType::class, array(
                "choices" => array(
                    "2020" => "2020",
                    "2021" => "2021",
                    "2022" => "2022",
                    "2023" => "2023",
                    "2024" => "2024",
                    "2025" => "2025",
                    "2026" => "2026",
                    "2027" => "2027",
                    "2028" => "2028",
                    "2029" => "2029",
                    "2030" => "2030",
                ),
                "label" => "Any del manteniment: ",
            ))
            ->add("fecha", TextType::class, ["label" => "Data (dd/mm/aaaa): "])
            ->add("horaEntrada", TextType::class, ["label" => "Hora d'entrada: "])
            ->add("horaSalida", TextType::class, ["label" => "Hora de sortida: "])
            ->add("horasTotales", ChoiceType::class, array(
                "choices" => array(
                    "0h 15m" => 0.25,
                    "0h 30m" => 0.5,
                    "0h 45m" =>0.75,
                    "1h" => 1.00,
                    "1h 15m" => 1.25,
                    "1h 30m" => 1.5,
                    "1h 45m" => 1.75,
                    "2h" => 2,
                    "2h 15m" => 2.25,
                    "2h 30m" => 2.5,
                    "2h 45m" => 2.75,
                    "3h" => 3,
                    "3h 15m" => 3.25,
                    "3h 30m" => 3.5,
                    "3h 45m" => 3.75,
                    "4h" => 4,
                    "4h 15m" => 4.25,
                    "4h 30m" => 4.5,
                    "4h 45m" => 4.75,
                    "5h" => 5,
                    "5h 15m" => 5.25,
                    "5h 30m" => 5.5,
                    "5h 45m" => 5.75,
                    "6h" => 6,
                ),
                "label" => "Hores totals: "
            ))
            ->add("tecnico", ChoiceType::class, array(
                "choices" => array(
                    "Oriol" => "Oriol",
                    "Víctor" => "Víctor",
                ),
                "label" => "Tecnic: "
            ))
            ->add("albaran", TextType::class, ["required" => false, "label" => "Albarà: "])
            ->add("observaciones", TextareaType::class, ["required" => false, "label" => "Observacions: "])
            ->add("submit", SubmitType::class, ["label" => "Guardar manteniment"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $equipo = $form->getData();
            $entityManager->persist($equipo);
            $entityManager->flush();

            return $this->redirectToRoute('inicio');
        }

        return $this->render("editarMantenimiento.html.twig", [
            'form' => $form->createView(),
        ]);

    }

}
