<?php


namespace App\Controller;

use App\Entity\Cliente;
use App\Entity\Equipo;
use App\Entity\Mantenimiento;
use Svg\Tag\Text;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FloatType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Sodium\add;

class ClientController extends AbstractController
{
    /**
     * @Route("/nou_client", name="nou_client")
     */
    public function crear_client(Request $request) {

        $client = new Cliente();

        $form = $this->createFormBuilder($client)
            ->add("nombre", TextType::class, ["label" => "Nom del client: "])
            ->add("direccion", TextType::class, array("label" => "Direcció: ", 'required' => false))
            ->add("codigoPostal", TextType::class, array("label" => "Codi postal: ", 'required' => false))
            ->add("municipio", TextType::class, array("label" => "Municipi: ", 'required' => false))
            ->add("provincia", ChoiceType::class, array(
                "choices" => [
                    "" => "",
                    "Barcelona" => "Barcelona",
                    "Girona" => "Girona",
                    "Lleida" => "Lleida",
                    "Tarragona" => "Tarragona",
                ],
                "label" => "Provincia: ",
            ))
            ->add("email", TextType::class, array("label" => "Email: ", 'required' => false))
            ->add("telefono", TextType::class, array("label" => "Telèfon: ", 'required' => false))
            ->add("tipusManteniment", ChoiceType::class, array(
                "choices" => [
                    "Mensual" => "Mensual",
                    "Bimestral" => "Bimestral",
                    "Trimestral" => "Trimestral",
                ]
            ))
            ->add("horasMantenimiento", NumberType::class, ["label" => "Hores de manteniment: "])
            ->add("submit", SubmitType::class, ["label" => "Crear Client"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $client = $form->getData();
            $entityManager->persist($client);
            $entityManager->flush();

            return $this->redirectToRoute('nou_client');
        }

        return $this->render("nouClient.html.twig", [
            'form' => $form->createView(),
            'origen' => 'nou_client'
        ]);
    }

    /**
     * @Route("/llistar_clients/{cliente}/editar/{id}", name="editarClient")
     */
    public function editarClient($cliente, $id, Request $request) {

        $entityManager = $this->getDoctrine()->getManager();
        $client = $entityManager->getRepository(Cliente::class)->find($id);

        $form = $this->createFormBuilder($client)
            ->add("nombre", TextType::class, ["label" => "Nom del client: "])
            ->add("direccion", TextType::class, array("label" => "Direcció: ", 'required' => false))
            ->add("codigoPostal", TextType::class, array("label" => "Codi postal: ", 'required' => false))
            ->add("municipio", TextType::class, array("label" => "Municipi: ", 'required' => false))
            ->add("provincia", ChoiceType::class, array(
                "choices" => [
                    "" => "",
                    "Barcelona" => "Barcelona",
                    "Girona" => "Girona",
                    "Lleida" => "Lleida",
                    "Tarragona" => "Tarragona",
                ],
                "label" => "Provincia: ",
            ))
            ->add("email", TextType::class, array("label" => "Email: ", 'required' => false))
            ->add("telefono", TextType::class, array("label" => "Telèfon: ", 'required' => false))
            ->add("tipusManteniment", ChoiceType::class, array(
                "choices" => [
                    "Mensual" => "Mensual",
                    "Bimestral" => "Bimestral",
                    "Trimestral" => "Trimestral",
                ]
            ))
            ->add("horasMantenimiento", NumberType::class, ["label" => "Hores de manteniment: "])
            ->add("submit", SubmitType::class, ["label" => "Guardar Client"])
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $client = $form->getData();
            $entityManager->persist($client);
            $entityManager->flush();

            return $this->redirectToRoute('inicio');
        }

        return $this->render("editarClient.html.twig", [
            'form' => $form->createView(),
        ]);
    }

    //Funcion que devuelve un listado con todos los clientes
    /**
     * @Route("/llistat_clients", name="llistat_clients")
     */
    public function mostrar_clients() {

        $repository = $this->getDoctrine()->getRepository(Cliente::class);
        $clientes = $repository->findAll();

        if($clientes) {
            return $this->render("listar.html.twig", [
                "clientes" => $clientes
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap client"
            );
        }
    }

    //Funcion que muestra toda la informacion de cada cliente
    /**
     * @Route("/llistat_clients/{cliente}", name="info_clients")
     */
    public function info_clients($cliente) {

        $repository = $this->getDoctrine()->getRepository(Cliente::class);
        $clientes = $repository->findBy(array("nombre" => $cliente));

        if($clientes) {
            return $this->render("cliente.html.twig", [
                "clientes" => $clientes
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap client"
            );
        }
    }

    //Funcion que muestra los diferentes años de manteniminetos
    /**
     * @Route("/llistat_clients/{cliente}/anys", name="año_mantenimiento")
     */
    public function años_mantenimientos($cliente) {

        $repository = $this->getDoctrine()->getRepository(Cliente::class);
        $clientes = $repository->findBy(array("nombre" => $cliente));

        if($clientes) {
            return $this->render("años_mantenimientos.html.twig", [
                "clientes" => $clientes
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "No hi ha cap client"
            );
        }

    }

    //Funcion que muestra el listado de equipos de cada empresa
    /**
     * @Route("/llistar_clients/{cliente}/equips", name="llistat_equips")
     */
    Public function llistat_equips($cliente, Request $request) {

        $found = $this->getDoctrine()
            ->getRepository(Cliente::class)
            ->createQueryBuilder("cliente")
            ->addSelect("cliente")
            ->andWhere("cliente.nombre = :cliente")
            ->setParameter("cliente", $cliente)
            ->getQuery()
            ->getResult()
        ;

        $equipos = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->createQueryBuilder("equipo")
            ->addSelect("equipo")
            ->andWhere("equipo.cliente = :id")
            ->setParameter("id", $found[0]->getId())
            ->getQuery()
            ->getResult();

        if($equipos) {
            return $this->render("equipos.html.twig", [
                "equipos" => $equipos,
                "cliente" => $cliente
            ]);
        }
        else {
            throw $this->createNotFoundException(
                "Aquest client no te cap equip"
            );
        }

    }

    //Funcion que muestra el listado de mantenimientos de cada empresa
    /**
     * @Route("/llistat_clients/{cliente}/anys/{ano}", name="llistat_manteniments")
     */
    Public function llistat_manteniments($cliente, $ano, Request $request) {

        $found = $this->getDoctrine()
            ->getRepository(Cliente::class)
            ->createQueryBuilder("cliente")
            ->addSelect("cliente")
            ->andWhere("cliente.nombre = :cliente")
            ->setParameter("cliente", $cliente)
            ->getQuery()
            ->getResult()
        ;

        $mantenimientos_enero = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Gener")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_enero);
        $totalEnero = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalEnero += $mantenimientos_enero[$i]->getHorasTotales();
        }

        $mantenimientos_febrero = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Febrer")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_febrero);
        $totalFebrero = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalFebrero += $mantenimientos_febrero[$i]->getHorasTotales();
        }

        $totalEneroFebrero = $totalEnero + $totalFebrero;

        $mantenimientos_marzo = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Març")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_marzo);
        $totalMarzo = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalMarzo += $mantenimientos_marzo[$i]->getHorasTotales();
        }

        $totalEneroFebreroMarzo = $totalEneroFebrero + $totalMarzo;

        $mantenimientos_abril = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Abril")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_abril);
        $totalAbril = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalAbril += $mantenimientos_abril[$i]->getHorasTotales();
        }

        $totalMarzoAbril = $totalMarzo + $totalAbril;

        $mantenimientos_mayo = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Maig")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_mayo);
        $totalMayo = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalMayo += $mantenimientos_mayo[$i]->getHorasTotales();
        }

        $mantenimientos_junio = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Juny")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_junio);
        $totalJunio = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalJunio += $mantenimientos_junio[$i]->getHorasTotales();
        }

        $totalMayoJunio = $totalMayo + $totalJunio;

        $totalAbrilMayoJunio = $totalAbril + $totalMayoJunio;

        $mantenimientos_julio = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Juliol")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_julio);
        $totalJulio = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalJulio += $mantenimientos_julio[$i]->getHorasTotales();
        }

        $mantenimientos_agosto = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Agost")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_agosto);
        $totalAgosto = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalAgosto += $mantenimientos_agosto[$i]->getHorasTotales();
        }

        $totalJulioAgosto = $totalJulio + $totalAgosto;

        $mantenimientos_septiembre = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Setembre")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_septiembre);
        $totalSeptiembre = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalSeptiembre += $mantenimientos_septiembre[$i]->getHorasTotales();
        }

        $totalJulioAgostoSeptiembre = $totalJulioAgosto + $totalSeptiembre;

        $mantenimientos_octubre = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Octubre")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_octubre);
        $totalOctubre = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalOctubre += $mantenimientos_octubre[$i]->getHorasTotales();
        }

        $totalSeptiembreOctubre = $totalSeptiembre + $totalOctubre;

        $mantenimientos_noviembre = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Novembre")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_noviembre);
        $totalNoviembre = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalNoviembre += $mantenimientos_noviembre[$i]->getHorasTotales();
        }

        $mantenimientos_diciembre = $this->getDoctrine()
            ->getRepository(Mantenimiento::class)
            ->createQueryBuilder("mantenimiento")
            ->addSelect("mantenimiento")
            ->andWhere("mantenimiento.cliente = :id")
            ->andWhere("mantenimiento.mes_mantenimiento = :mes")
            ->andWhere("mantenimiento.ano_mantenimiento = :ano")
            ->setParameter("id", $found[0]->getId())
            ->setParameter("mes", "Desembre")
            ->setParameter("ano", $ano)
            ->getQuery()
            ->getResult();

        $longitudArray = count($mantenimientos_diciembre);
        $totalDiciembre = 0.0;
        for ($i = 0; $i < $longitudArray; ++$i) {
            $totalDiciembre += $mantenimientos_diciembre[$i]->getHorasTotales();
        }

        $totalNoviembreDiciembre = $totalNoviembre + $totalDiciembre;

        $totalOctubreNoviembreDiciembre = $totalOctubre + $totalNoviembreDiciembre;

        if($mantenimientos_enero  || $mantenimientos_febrero || $mantenimientos_marzo || $mantenimientos_abril || $mantenimientos_mayo || $mantenimientos_junio || $mantenimientos_julio || $mantenimientos_agosto || $mantenimientos_septiembre || $mantenimientos_octubre || $mantenimientos_noviembre || $mantenimientos_diciembre) {

            if($found[0]->getTipusManteniment() == "Mensual") {

                return $this->render("mantenimientos_mensuales.html.twig", [
                    "mantenimientos_enero" => $mantenimientos_enero,
                    "mantenimientos_febrero" => $mantenimientos_febrero,
                    "mantenimientos_marzo" => $mantenimientos_marzo,
                    "mantenimientos_abril" => $mantenimientos_abril,
                    "mantenimientos_mayo" => $mantenimientos_mayo,
                    "mantenimientos_junio" => $mantenimientos_junio,
                    "mantenimientos_julio" => $mantenimientos_julio,
                    "mantenimientos_agosto" => $mantenimientos_agosto,
                    "mantenimientos_septiembre" => $mantenimientos_septiembre,
                    "mantenimientos_octubre" => $mantenimientos_octubre,
                    "mantenimientos_noviembre" => $mantenimientos_noviembre,
                    "mantenimientos_diciembre" => $mantenimientos_diciembre,
                    "cliente" => $found[0],
                    "totalEnero" => $totalEnero,
                    "totalFebrero" => $totalFebrero,
                    "totalMarzo" => $totalMarzo,
                    "totalAbril" => $totalAbril,
                    "totalMayo" => $totalMayo,
                    "totalJunio" => $totalJunio,
                    "totalJulio" => $totalJulio,
                    "totalAgosto" => $totalAgosto,
                    "totalSeptiembre" => $totalSeptiembre,
                    "totalOctubre" => $totalOctubre,
                    "totalNoviembre" => $totalNoviembre,
                    "totalDiciembre" => $totalDiciembre,
                ]);

            }
            elseif ($found[0]->getTipusManteniment() == "Bimestral") {

                return $this->render("mantenimientos_bimensual.html.twig", [
                    "mantenimientos_enero" => $mantenimientos_enero,
                    "mantenimientos_febrero" => $mantenimientos_febrero,
                    "mantenimientos_marzo" => $mantenimientos_marzo,
                    "mantenimientos_abril" => $mantenimientos_abril,
                    "mantenimientos_mayo" => $mantenimientos_mayo,
                    "mantenimientos_junio" => $mantenimientos_junio,
                    "mantenimientos_julio" => $mantenimientos_julio,
                    "mantenimientos_agosto" => $mantenimientos_agosto,
                    "mantenimientos_septiembre" => $mantenimientos_septiembre,
                    "mantenimientos_octubre" => $mantenimientos_octubre,
                    "mantenimientos_noviembre" => $mantenimientos_noviembre,
                    "mantenimientos_diciembre" => $mantenimientos_diciembre,
                    "cliente" => $found[0],
                    "totalEneroFebrero" => $totalEneroFebrero,
                    "totalMarzoAbril" => $totalMarzoAbril,
                    "totalMayoJunio" => $totalMayoJunio,
                    "totalJulioAgosto" => $totalJulioAgosto,
                    "totalSeptiembreOctubre" => $totalSeptiembreOctubre,
                    "totalNoviembreDiciembre" => $totalNoviembreDiciembre,
                ]);

            }
            else {

                return $this->render("mantenimientos_trimestral.html.twig", [
                    "mantenimientos_enero" => $mantenimientos_enero,
                    "mantenimientos_febrero" => $mantenimientos_febrero,
                    "mantenimientos_marzo" => $mantenimientos_marzo,
                    "mantenimientos_abril" => $mantenimientos_abril,
                    "mantenimientos_mayo" => $mantenimientos_mayo,
                    "mantenimientos_junio" => $mantenimientos_junio,
                    "mantenimientos_julio" => $mantenimientos_julio,
                    "mantenimientos_agosto" => $mantenimientos_agosto,
                    "mantenimientos_septiembre" => $mantenimientos_septiembre,
                    "mantenimientos_octubre" => $mantenimientos_octubre,
                    "mantenimientos_noviembre" => $mantenimientos_noviembre,
                    "mantenimientos_diciembre" => $mantenimientos_diciembre,
                    "cliente" => $found[0],
                    "totalEneroFebreroMarzo" => $totalEneroFebreroMarzo,
                    "totalAbrilMayoJunio" => $totalAbrilMayoJunio,
                    "totalJulioAgostoSeptiembre" => $totalJulioAgostoSeptiembre,
                    "totalOctubreNoviembreDiciembre" => $totalOctubreNoviembreDiciembre,
                ]);

            }

        }
        else {
            throw $this->createNotFoundException(
                "Aquest client no te cap manteniment"
            );
        }

    }

}
