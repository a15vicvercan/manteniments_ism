<?php


namespace App\Controller;


use App\Entity\Cliente;
use App\Entity\Equipo;
use App\Entity\Mantenimiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Dompdf\Options;
use Dompdf\Dompdf;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Knp\Bundle\SnappyBundle\Snappy\Response\PdfResponse;

class PdfController extends Controller
{

    /**
     * @Route("/generate-pdf/{cliente}", name="app_generate_pdf")
     */
    public function generarPdf($cliente, Request $request) {

        $found = $this->getDoctrine()
            ->getRepository(Cliente::class)
            ->createQueryBuilder("cliente")
            ->addSelect("cliente")
            ->andWhere("cliente.nombre = :cliente")
            ->setParameter("cliente", $cliente)
            ->getQuery()
            ->getResult()
        ;

        $equipos = $this->getDoctrine()
            ->getRepository(Equipo::class)
            ->createQueryBuilder("equipo")
            ->addSelect("equipo")
            ->andWhere("equipo.cliente = :id")
            ->setParameter("id", $found[0]->getId())
            ->getQuery()
            ->getResult();

        if($equipos) {

            $pdfOptions = new Options();
            $pdfOptions->set("defaultFont", "Arial");

            $dompdf = new Dompdf($pdfOptions);

            $html = $this->renderView("pdf.html.twig", [
                "title" => "Welcome to our PDF Test",
                "equipos" => $equipos,
                "cliente" => $cliente
            ]);

            $dompdf->loadHtml($html);

            $dompdf->setPaper("A4", "landscape");

            $dompdf->render();

            $dompdf->stream("Llistat_equips.pdf", [
                "Attachment" => true
            ]);

        }

    }


    /**
     * @Route("/llistar_clients/{cliente}/manteniments/generarPDF/{id}", name="generarMantenimientoPdf")
     */
    public function generarMantenimentPdf($cliente, $id, Request $request, \Swift_Mailer $mailer) {

        $repository = $this->getDoctrine()->getRepository(Mantenimiento::class);
        $mantenimiento = $repository->findBy(array("id" => $id));

        if($mantenimiento) {

            $pdfOptions = new Options();
            $pdfOptions->set("defaultFont", "Arial");

            $dompdf = new Dompdf($pdfOptions);

            $html = $this->renderView("mantenimiento.html.twig", [
                "mantenimiento" => $mantenimiento,
                "cliente" => $cliente,
            ]);

            $dompdf->loadHtml($html);

            $dompdf->setPaper("A4", "portrait");

            $dompdf->render();

            $dompdf->stream("Manteniment-" . $id . ".pdf", [
                "Attachment" => true
            ]);

        }
        else {
            throw $this->createNotFoundException(
                "No hi ha informació d'aquest equip"
            );
        }

    }
}